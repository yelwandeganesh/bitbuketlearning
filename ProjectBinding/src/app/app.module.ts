import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'; 
import { AppComponent } from './app.component';
import { MarvellousCompComponent } from './marvellous-comp/marvellous-comp.component';
import { TempComponent } from './temp/temp.component';
import { NgifComponent } from './ngif/ngif.component';
import { InputparaComponent } from './inputpara/inputpara.component';
import { NgmodelComponent } from './ngmodel/ngmodel.component';

@NgModule({
  declarations: [
    AppComponent,
    MarvellousCompComponent,
    TempComponent,
    NgifComponent,
    InputparaComponent,
    NgmodelComponent
  ],
  imports: [
    BrowserModule,FormsModule
  ],

  providers: [],

  bootstrap: [AppComponent,TempComponent,NgifComponent,InputparaComponent,NgmodelComponent]
})
export class AppModule { }
