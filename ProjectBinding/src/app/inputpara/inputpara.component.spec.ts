import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputparaComponent } from './inputpara.component';

describe('InputparaComponent', () => {
  let component: InputparaComponent;
  let fixture: ComponentFixture<InputparaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputparaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputparaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
