import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'ngmodel-app',
  template: `
    <h3>NgModel works!</h3>
    <input [(ngModel)]="name" #ctrl="ngModel" required>

    <p>Value: {{ name }}</p>
    <p>Valid ctrl: {{ ctrl.valid }}</p>
    
    <button (click)="setValue()">Set value</button>
  `,
})
export class NgmodelComponent implements OnInit {


  name: string = '';

  setValue() { this.name = 'Nancy'; }
  constructor() { }

  ngOnInit(): void {
  }

}
